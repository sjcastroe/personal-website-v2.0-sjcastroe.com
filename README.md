# **sjcastroe.com** #

## **About** ##
sjcastroe.com is my personal profile consisting of a mission statement and a reference to my work for fellow software engineers and potential employers. The website originally began as practice for front end web development using JavaScript and jQuery. It is split into five sections accessed through a simple auto scroll interface and supports responsive design implemented through media queries.

### Home ###
![home.jpg](https://bitbucket.org/repo/nbo4j7/images/2031656695-home.jpg)
### What I Do ###
![whatido.jpg](https://bitbucket.org/repo/nbo4j7/images/1529272158-whatido.jpg)
### How I Do It ###
![howidoit.jpg](https://bitbucket.org/repo/nbo4j7/images/2304705079-howidoit.jpg)
### Projects ###
![projects.jpg](https://bitbucket.org/repo/nbo4j7/images/201252273-projects.jpg)
### Contact ###
![contact.jpg](https://bitbucket.org/repo/nbo4j7/images/450454006-contact.jpg)

## **Technologies** ##
- Created with HTML5, CSS3, JavaScript, and PHP
- html5doctor.com Reset Stylesheet v1.6.1 courtesy of Richard Clark http://richclarkdesign.com
- JavaScript with JQuery v2.1.1 http://jquery.com/
- enquire.js courtesy of Nick Williams http://wicky.nillia.ms/enquire.js
- popup.js courtesy of Nicolas Höning http://nicolashoening.de?twocents&nr=8
- -prefix-free courtesy of Lea Verou http://leaverou.github.io/prefixfree/
- MathJax for math typesetting http://www.mathjax.org/
- Original graphics created with Adobe Photoshop CS6 and Illustrator CS6
- Git with SourceTree GUI for source control http://www.sourcetreeapp.com/
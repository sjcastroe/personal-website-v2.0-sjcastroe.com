<?php
if( !empty($_POST) )
{
	$errors = '';
	$sender = 'guest@sjcastroe.com';
	if(empty($_POST['name'])  || 
	   empty($_POST['email']) || 
	   empty($_POST['message']))
	{
		$errors .= "All fields are required.<br/>";
	}

	$name = $_POST['name'];
	$email_address = $_POST['email'];
	$message = $_POST['message']; 

	if (!preg_match(
	"/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", 
	$email_address))
	{
		$errors .= "Invalid email address.<br/>";
	}

	if( empty($errors) )
	{
		$to = 'sjcastroe@gmail.com'; 
		$email_subject = "New message from sjcastroe.com: $name";
		$email_body = "You have received a new message. ".
		" Here are the details:\n Name: $name \n Email: $email_address \n Message: \n $message"; 
		
		$headers = "From: $sender\n";
		$headers .= "Reply-To: $email_address";
		
		if( mail($to,$email_subject,$email_body,$headers) )
			$success = "Message successfully sent.";
		else
			$success ="Message sending failed. Please try again.";
	}
	else {
		$success = $errors;
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Sergio Castro - Mathematical Programmer</title>
	<meta charset="UTF-8">
	<meta name="Author" content="Sergio Castro">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="format-detection" content="telephone=no">
	
	<script type="text/javascript" src="js/enquire.min.js"></script>
	<script type="text/javascript" src="js/jquery-2.1.1.js"></script>
	<script type="text/javascript" src="js/nhpup_1.1.js"></script>
	<script type="text/javascript" src="js/prefixfree.min.js"></script>
	<script type="text/javascript" src="js/sjcastroe.js"></script>

	<link rel="stylesheet" type="text/css" href="css/html5reset-1.6.1.css">
	<link rel="stylesheet" type="text/css" href="css/sjcastroe_desktop.css" media="all and (min-width: 1280px)">
	<link rel="stylesheet" type="text/css" href="css/sjcastroe_mobile.css" media="all and (max-width: 1279px)">
	
</head>

<body>
	<div id="board" class="menu_item">
		<div data-orientation="vertical" data-direction="-" data-speed="0.05">
		</div>
	</div>
	<div id="projector">
		<div id="projector_screen" class="group_hor_cent" data-orientation="vertical" data-direction="+" data-speed="1">
		</div>
	</div>
	<div id="auto_scroll_up" class="auto_scroll">
		<div id="arrow_up"></div>
		<div id="message_up">[click to return]</div>
	</div>
	<div id="auto_scroll_down" class="auto_scroll">
		<div id="arrow_down"></div>
		<div id="message_down">[click to continue]</div>
	</div>
	<div id="home" class="menu_item 1" data-scroll="1">
		<div id="container" class="center">
			<div class="vert_cent"><!-- Controls padding in between sections -->
				<div class="group_hor_cent">	
					<div id="home_decor" class="hor_cent"><!-- Controls padding within a section (horizontal), Add padding-top to next adjacent element -->
						<div class="hor_cent_2">
							<div class="hor_cent_3">
								<div id="displace" class="displace"><!-- Set adjustments for a specific img -->
									<img src="images/home_decor.png" alt="home_decoration">
								</div>
							</div>
						</div>
					</div>
					<div id="home_main_title" class="hor_cent">
						<div class="hor_cent_2">
							<div class="hor_cent_3" data-type="parallax_parent" data-orientation="vertical" data-direction="+" data-speed="1">
								<div id="displace" class="displace">
									<div data-type="parallax" data-orientation="vertical" data-direction="-" data-speed="1"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="about_1" class="menu_item 1 2" data-scroll="2">
		<div id="container" class="center">
			<div class="vert_cent">
				<div class="group_hor_cent">
					<div id="content" class="hor_cent">
						<div class="hor_cent_2">
							<div class="hor_cent_3">
								<div id="displace" class="displace">
									<div>
										I suppose I'm using the term loosely. Mathematical programming has its own formal 
										definition within the field of mathematics, and it's not directly related to computer 
										programming. But what more appropriate title conveys a merging of rigorous computing 
										theory and quality software design, of computer science and software engineering, of 
										theory and practice? That is to say, what else would you call the person who develops
										the <span style="color: #ff6868">mathematically powered</span> mechanisms that drive 
										our software systems?
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="what_i_do" class="hor_cent">
						<div id="what_i_do_sign"></div>
						<div class="hor_cent_2">
							<div class="hor_cent_3" data-type="parallax_parent" data-orientation="vertical" data-direction="+" data-speed="1">
								<div id="displace" class="displace">
									<div id="what_i_do_arrow" data-type="parallax" data-orientation="vertical" data-direction="-" data-speed="1"></div>
								</div>
							</div>
						</div>
					</div>
					<div id="skills_i_have" class="hor_cent">
						<div class="hor_cent_2">
							<div class="hor_cent_3">
								<div id="displace" class="displace">
									<img src="images/skills_i_have.png" alt="skills_i_have">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="about_2" class="menu_item 2 3" data-scroll="3">
		<div id="container" class="center">
			<div class="vert_cent">
				<div class="group_hor_cent">
					<div id="content" class="hor_cent">
						<div class="hor_cent_2">
							<div class="hor_cent_3">
								<div id="displace" class="displace">
									<div>
										I'm a UCLA graduate in <span style="color: #3b88fc">applied mathematics</span> with 
										a specialization in <span style="color: #fce629">computing</span>, 
										and it pains me to hear developers assert that mathematics is not needed in 
										industry. To claim the computing industry has outgrown the work of the mathematical 
										logicians from which it birthed is to undermine the areas of greatest innovation in 
										the field of computing. Mathematics prevails in these areas, which include, but are 
										not limited to, cryptography, image processing, compression, genetic algorithms, 
										algorithm analysis, 3D graphics, and artificial intelligence. Even internet search 
										engines, a staple of the modern internet experience, depend on <span style="color:#ff3636">mathematical algorithms</span> 
										to index and rank the information on the Web. For example, Google's trademark PageRank 
										algorithm employs principles of linear algebra, namely matrix multiplication and eigenvectors. 
										Software engineers may run the course of their careers without using an ounce of math, but for 
										every truly remarkable achievement in computing, mathematics is at the helm. When it's time to 
										innovate, <span style="color: #59e129">mathematical ingenuity</span> distinguishes a 
										<em>great</em> programmer from a good one.
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="how_i_do_it" class="hor_cent">
						<div id="what_i_love_sticky">
							<div id="what_i_love_desktop" class="pop_up" onmouseover="nhpup.popup($('#what_i_love_list').html(), {'class': 'what_i_love_list', 'width': 230});"></div>
						</div>
						<div id="how_i_do_it_sign"></div>
						<div class="hor_cent_2">
							<div class="hor_cent_3" data-type="parallax_parent" data-orientation="vertical" data-direction="+" data-speed="1">
								<div id="displace" class="displace">
									<div id="how_i_do_it_graph" data-type="parallax" data-orientation="vertical" data-direction="-" data-speed="1">
										<div id="define" class="pop_up" onmouseover="nhpup.popup('Address the need for a program and analyze the requirements. What is the problem to be solved and its boundaries?');"></div>
										<div id="model" class="pop_up" onmouseover="nhpup.popup('What basic strategy will be employed to solve the problem? Brainstorm potential solutions. Construct a flowchart identifying major components and their interactions.');"></div>
										<div id="design" class="pop_up" onmouseover="nhpup.popup('Split components into modules that conform to the object-oriented principles of SOLID. Create a rapid prototype if necessary.');"></div>
										<div id="refine" class="pop_up" onmouseover="nhpup.popup('Have our modules been abstracted enough? Are we coding to interfaces rather than implementations? This step is crucial for maintainability and will often be revisited.');"></div>
										<div id="analyze" class="pop_up" onmouseover="nhpup.popup('Is the chosen solution ideal? Assess the potential limitations of the design to fully understand its costs and benefits.');"></div>
										<div id="implement" class="pop_up" onmouseover="nhpup.popup('With a structured and resource efficient plan, code up a clean solution. If all previous steps were performed correctly, this should be painless.');"></div>
										<div id="hover_message">[hover to learn more]</div>
										<div id="what_i_love_mobile" class="pop_up" onmouseover="nhpup.popup($('#what_i_love_list').html(), {'class': 'what_i_love_list', 'width': 230});"></div>
										<div id="what_i_love_list" style="display: none;">
											<ul>
												<li>Math</li>
												<li>Solving Difficult Problems</li>
												<li>Singing & Guitar</li>
												<li>Weight Training</li>
												<li>Coding</li>
												<li>Game Development</li>
												<li>Tutoring</li>
												<li>Graphic Design</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="projects" class="menu_item 3 4" data-scroll="4">
		<div id="container" class="center">
			<div class="vert_cent">
				<div class="group_hor_cent">
					<div id="pursuit" class="hor_cent">
						<div class="hor_cent_2">
							<div class="hor_cent_3">
								<div id="displace" class="displace">
									<div>
										My pursuit as a professional is the convergence of mathematics, computer science, and software engineering to build efficient and maintainable software.
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="bitbucket" class="hor_cent">
						<div class="hor_cent_2">
							<div class="hor_cent_3" data-type="parallax_parent" data-orientation="vertical" data-direction="+" data-speed="1">
								<div id="displace" class="displace">
									<div id="bitbucket_folder" data-type="parallax" data-orientation="vertical" data-direction="-" data-speed="1">
										<a href="https://bitbucket.org/sjcastroe"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="photo" class="hor_cent">
						<div class="hor_cent_2">
							<div class="hor_cent_3">
								<div id="displace" class="displace">
									<img src="images/photo.png" alt="photo">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="contact" class="menu_item 4" data-scroll="5">
		<div id="container" class="center">
			<div class="vert_cent">
				<div class="group_hor_cent">
					<div id="envelope" class="hor_cent">
						<div class="hor_cent_2">
							<div class="hor_cent_3" data-type="parallax_parent" data-orientation="vertical" data-direction="+" data-speed="1">
								<div id="displace" class="displace">
									<div data-type="parallax" data-orientation="vertical" data-direction="-" data-speed="1">
										<div id="form_container">
											<form id="contact_form" class="contact_form" action="#contact" method="post" name="contact_form" >
												<ul>
													<li><input type="text" name="name" placeholder="Your Full Name" required /></li>
													<li><input type="email" name="email" placeholder="Your E-mail" required pattern="^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$"/></li>
													<li id="text_area"><textarea name="message" placeholder="Your Message" required ></textarea></li>
													<li id="send_button">
														<button class="submit send" type="submit" name="contactSubmit" id="contactSubmit">Send</button>
														<?php
															if( isset($success) )
															{
																if($success == "Message successfully sent.")
																	echo "<p id=" . '"success_message"' . ">" . $success . "</p>";
																else
																	echo "<p id=" . '"fail_message"' . ">" . $success . "</p>";
															}
														?>
													</li>
												</ul>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="contact_info" class="hor_cent">
						<div class="hor_cent_2">
							<div class="hor_cent_3">
								<div id="displace" class="displace">
									<div>
										<address>
											<ul>
												<li>Email: sjcastroe@gmail.com</li>
												<li>Skype: sjcastroe</li>
												<li>Cell no: <span>858.</span><span>568.1727</span></li>
											</ul>
										</address>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer>
			<small id="copyright">© 2014 sjcastroe. All rights reserved.</small>
			<small id="design_by"><span> web design by</span> Sergio Castro<span style="font-style:italic"> : projecting on a black board.</small>
		</footer>
	</div>
</body>

</html>

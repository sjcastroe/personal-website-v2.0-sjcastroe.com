window.onload = function () {
	window.current_window = $(window).scrollTop()/$("#home").height() + 1;//global variable keeps track of current window
	start_projector();
	fire_enquire();
}

function fire_enquire()
{
	enquire.register("all and (min-width: 1280px)", {
		match : function() {
			start_projector();
			$(window).scroll(function() {
				current_window = $(window).scrollTop()/$("#home").height() + 1;//updates current window global
			});
			$(window).resize(function() {
				current_window = $(window).scrollTop()/$("#home").height() + 1;
			});
			parallax_scroll_page('on');
			run_auto_scroll_interface('on');
		},
		unmatch : function() {
			parallax_scroll_page('off');
			run_auto_scroll_interface('off');
		}
	});
}

//Start projector
function start_projector()
{
	$("#projector").show();
	var pos = ($('body').height() * 0.5) - 290 + "px";
	$("#projector").css({top: pos});
}

//PARALLAX SCROLLING
function parallax_scroll_page(mode)
{
	if( mode === 'on' )
	{
		parallax_scroll_items();
		$(window).on("scroll", function() {
			parallax_scroll_items('do');
		});
		
		$(window).on("resize", function() {
			parallax_scroll_items('do');
		});
	}
	else
	{
		parallax_scroll_items('undo');
		$(window).off("scroll");
		$(window).off("resize");
	}
}

function parallax_scroll_items(mode)
{
	if( mode === 'do')
	{
		var pair = current_window | 0;//Choose which pair of menu items to scroll
		var selector_str = '.' + pair + ' div[data-type="parallax"], .' + pair + ' img[data-type="parallax"]';
		$(selector_str).each(function() {
			parallax_scroll(mode, $(this), $(this).parent().parent().parent().parent().parent().parent().parent().parent());
			parallax_scroll(mode, $(this).parent().parent(), $(this).parent().parent().parent().parent().parent().parent().parent().parent());
		});
		
		var page_end = $('body').height() * 4;
		if($(window).scrollTop() < page_end)
		{
			parallax_scroll(mode, $("#board div"), $('body'));
			parallax_scroll(mode, $("#projector_screen"), $('body'));
		}
	}
	else
	{
		var selector_str = 'div[data-type="parallax"], img[data-type="parallax"], div[data-type="parallax_parent"]';
		$(selector_str).each(function() {
			parallax_scroll(mode, $(this), $(this));
		});
	}
}

/*Object is parallax scrolled relative to rel_obj.
Object should have these html attributes:
data-speed, data-direction, data-orientation
*/
function parallax_scroll(mode, object, rel_obj)//mode has two states: 'do' or 'undo'
{
	var coordinates = '';
	var pos = 0;
	var background_obj = object;
	if( mode === 'do' )
	{
		pos = ($(window).scrollTop() - rel_obj.offset().top) * background_obj.data('speed');
		if(background_obj.data('direction') == "-")//negative direction means up/left motion
					pos = -1 * pos;
	}
	coordinates = pos + 'px';
	if(background_obj.data('orientation') == "vertical") {
		background_obj.css({top: coordinates});
	}
	else {
		background_obj.css({left: coordinates});
	}
}

//AUTO_SCROLLING
function run_auto_scroll_interface(mode)
{
	hide_click_on_load(mode);
	hide_click_on_scroll(mode);

	if(mode === 'on')
	{
		$("#auto_scroll_up").on({
			mouseenter: function(){
				show_arrow(mode, $('#arrow_up'), $('#message_up'), 1); 
			},
			mouseleave: function(){
				hide_arrow(mode, $('#arrow_up'), $('#message_up'), 1);
			}
		});
		$("#auto_scroll_down").on({
			mouseenter: function(){
				show_arrow(mode, $('#arrow_down'), $('#message_down'), 5); 
			},
			mouseleave: function(){
				hide_arrow(mode, $('#arrow_down'), $('#message_down'), 5);
			}
		});
	}
	else
	{
		hide_arrow(mode, $('#arrow_up'), $('#message_up'), 1);
		hide_arrow(mode, $('#arrow_down'), $('#message_down'), 5);
		$("#auto_scroll_up").off('mouseenter').off('mouseleave');
		$("#auto_scroll_down").off('mouseenter').off('mouseleave');
	}
	
	if(mode === 'on')
	{
		$(window).on("scroll", function() {
			$(".auto_scroll").css("margin-left", -$(document).scrollLeft());
			if($('#auto_scroll_up:hover').length != 0)
				show_arrow(mode, $('#arrow_up'), $('#message_up'), 1);
			if($('#auto_scroll_down:hover').length != 0)
				show_arrow(mode, $('#arrow_down'), $('#message_down'), 5);
		});
	}
	else
	{
		show_arrow(mode, $('#arrow_up'), $('#message_up'), 1);
		show_arrow(mode, $('#arrow_down'), $('#message_down'), 5);
		$(window).off("scroll");
	}

	
	$("#auto_scroll_up")[0].onclick = function() { auto_scroll('up') };
	$("#auto_scroll_down")[0].onclick = function() { auto_scroll('down') };
}

function auto_scroll (direction)
{
	if (direction == 'down')
	{
		if (current_window < 5)
		{
			current_window = current_window | 0;
			var next_item = current_window + 1;
		}
		else
			return;
	}
	else
	{
		if (current_window % 1 == 0)
		{
			current_window = current_window | 0;
			var next_item = current_window - 1;
		}
		else
		{
			current_window = current_window | 0;
			var next_item = current_window;
		}
	}
	var selector_str = 'div[data-scroll="' + next_item + '"]';
	var scroll_dest = $(selector_str).offset().top;
	if(!(scroll_dest % 1 === 0))
		scroll_dest = scroll_dest + 1 | 0;
	$('html, body').animate(
		{scrollTop: scroll_dest},
		2500
	);
}

function show_arrow(mode, arrow, message, arrow_exception)
{
	if( mode === 'on')
	{
		message.hide();
		cur_win = current_window;
		if(cur_win > 5)
			cur_win = cur_win | 0;
		if(cur_win == arrow_exception)
			arrow.hide();
		else
			arrow.show();
	}
	else
	{
		message.hide();
		arrow.hide();
	}
}

function hide_arrow(mode, arrow, message, arrow_exception)
{
	if(mode === 'on')
	{
		arrow.hide();
		cur_win = current_window;
		if(cur_win > 5)
			cur_win = cur_win | 0;
		if(cur_win == arrow_exception)
			message.hide();
		else
			message.show();
	}
	else
	{
		arrow.hide();
		message.hide();
	}
}

function hide_click_on_scroll(mode)
{
	if (mode === "on")
	{
		$(window).on("scroll", function() {
			if (current_window == 1)
				$('#message_up').hide();
			else
				$('#message_up').show();
			if (current_window >= 5)
				$('#message_down').hide();
			else
				$('#message_down').show();
		});
	}
	else
	{
		$('#message_up').hide();
		$('#message_down').hide();
		$(window).off("scroll");
	}
}

function hide_click_on_load(mode)
{
	hide_arrow(mode, $('#arrow_up'), $('#message_up'), 1);
	hide_arrow(mode, $('#arrow_down'), $('#message_down'), 5);
}